# Moment default keybindings list

Keybindings as defined in [the default configuration file](src/config/settings.py#L218). More about configuration [here](docs/CONFIG.md).

## General bindings

Key | Function
------ | ------
<kbd>Ctrl + Alt + C</kbd> | Toggle compact interface
<kbd>Ctrl + +</kbd> | Zoom in
<kbd>Ctrl + -</kbd> | Zoom out
<kbd>Ctrl + =</kbd> | Reset zoom
<kbd>Alt + Shift + Left</kbd> <br> <kbd>Alt + Shift + H</kbd> | Previous tab
<kbd>Alt + Shift + Right</kbd> <br> <kbd>Alt + Shift + L</kbd> | Next tab
<kbd>Ctrl + Tab</kbd> | Switch to the last opened page
<kbd>Ctrl + H</kbd> | Earlier page in history (page back)
<kbd>Ctrl + L</kbd> | Later page in history (page forward)
<kbd>Ctrl + Alt + H</kbd> | Toggle notifications, except highlights
<kbd>Ctrl + Alt + N</kbd> | Toggle notifications
<kbd>F1</kbd> | QML developer console
<kbd>Shift + F1</kbd> | Python debugger
<kbd>Alt + F1</kbd> | Python remote debugger
<kbd>Ctrl + Q</kbd> | Quit Moment *

## Scrolling bindings

Key | Function
------ | ------
<kbd>Alt + Up</kbd> <br> <kbd>Alt + K</kbd> | Scroll up
<kbd>Alt + Down</kbd> <br> <kbd>Alt + J</kbd> | Scroll down
<kbd>Ctrl + Alt + Up</kbd> <br> <kbd>Ctrl + Alt + K</kbd> <br> <kbd>PgUp</kbd> | Page up
<kbd>Ctrl + Alt + Down</kbd> <br> <kbd>Ctrl + Alt + J</kbd> <br> <kbd>PgDown</kbd> | Page down
<kbd>Ctrl + Alt + Shift + Up</kbd> <br> <kbd>Ctrl + Alt + Shift + K</kbd> <br> <kbd>Home</kbd> | Scroll to top
<kbd>Ctrl + Alt + Shift + Down</kbd> <br> <kbd>Ctrl + Alt + Shift + J</kbd> <br> <kbd>End</kbd> | Scroll to bottom

## Account bindings

Key | Function
------ | ------
<kbd>Alt + Shift + A</kbd> | Add new account
<kbd>Alt + O</kbd> | Collapse current account
<kbd>Alt + A</kbd> | Current account settings
<kbd>Alt + P</kbd> | Current account context menu
<kbd>Ctrl + Alt + U</kbd> <br> <kbd>Ctrl + Alt + A</kbd> | Unavailable status
<kbd>Ctrl + Alt + I</kbd> | Invisible status
<kbd>Ctrl + Alt + O</kbd> | Offline status
<kbd>Alt + Shift + N</kbd> | Previous account
<kbd>Alt + N</kbd> | Next account
<kbd>Ctrl + 1</kbd> | Switch to account 1
<kbd>Ctrl + 2</kbd> | Switch to account 2
<kbd>Ctrl + 3</kbd> | Switch to account 3
<kbd>Ctrl + 4</kbd> | Switch to account 4
<kbd>Ctrl + 5</kbd> | Switch to account 5
<kbd>Ctrl + 6</kbd> | Switch to account 6
<kbd>Ctrl + 7</kbd> | Switch to account 7
<kbd>Ctrl + 8</kbd> | Switch to account 8
<kbd>Ctrl + 9</kbd> | Switch to account 9
<kbd>Ctrl + 0</kbd> | Switch to account 10

## Room bindings

Key | Function
------ | ------
<kbd>Alt + C</kbd> | Create a new room (start chat)
<kbd>Alt + F</kbd> <br> <kbd>Ctrl + K</kbd> | Focus filter *
<kbd>Alt + Shift + F</kbd> | Clear filter
<kbd>Alt + Shift + Up</kbd> <br> <kbd>Alt + Shift + K</kbd> | Previous room
<kbd>Alt + Shift + Down</kbd> <br> <kbd>Alt + Shift + J</kbd> | Next room
<kbd>Alt + Shift + U</kbd> | Previous unread
<kbd>Alt + U</kbd> | Next unread
<kbd>Ctrl + Shift + U</kbd> | Oldest unread
<kbd>Ctrl + U</kbd> | Latest unread
<kbd>Alt + Shift + M</kbd> | Previous highlight
<kbd>Alt + M</kbd> | Next highlight
<kbd>Ctrl + Shift + M</kbd> | Oldest highlight
<kbd>Ctrl + M</kbd> | Latest highlight
<kbd>Alt + 1</kbd> | Room number 1 in account
<kbd>Alt + 2</kbd> | Room number 2 in account
<kbd>Alt + 3</kbd> | Room number 3 in account
<kbd>Alt + 4</kbd> | Room number 4 in account
<kbd>Alt + 5</kbd> | Room number 5 in account
<kbd>Alt + 6</kbd> | Room number 6 in account
<kbd>Alt + 7</kbd> | Room number 7 in account
<kbd>Alt + 8</kbd> | Room number 8 in account
<kbd>Alt + 9</kbd> | Room number 9 in account
<kbd>Alt + 0</kbd> | Room number 10 in account
(no binding) | Jump to specific room by ID

## Chat bindings

Key | Function
------ | ------
<kbd>Alt + R</kbd> | Focus room pane
<kbd>Ctrl + Alt + R</kbd> | Hide room pane
<kbd>Alt + I</kbd> | Invite members
<kbd>Alt + Escape</kbd> | Leave current chat
<kbd>Alt + S</kbd> | Upload file
<kbd>Alt + Shift + S</kbd> | Send file at clipboard path

## Message bindings

Key | Function
------ | ------
<kbd>Ctrl + Up</kbd> <br> <kbd>Ctrl + I</kbd> | Focus previous message *
<kbd>Ctrl + Down</kbd> <br> <kbd>Ctrl + J</kbd> | Focus next message
<kbd>Ctrl + Space</kbd> | Select focused message
<kbd>Ctrl + Shift + Space</kbd> | Select messages until here
<kbd>Ctrl + D</kbd> | Unfocus or deselect
<kbd>Ctrl + S</kbd> | Display seen tooltips
<kbd>Ctrl + Shift + R</kbd> <br> <kbd>Alt + Del</kbd> | Remove message *
<kbd>Ctrl + R</kbd> | Reply *
<kbd>Ctrl + Shift + D</kbd> | Debug message
<kbd>Ctrl + O</kbd> | Open link/file in message
<kbd>Ctrl + Shift + O</kbd> | Open link/file externally
<kbd>Ctrl + Shift + C</kbd> | Copy downloaded file path
<kbd>Ctrl + Shift + L</kbd> | Clear messages

## Image viewer bindings

Key | Function
------ | ------
<kbd>X</kbd> <br> <kbd>Q</kbd> | Close image viewer
<kbd>E</kbd> | Expand image viewer
<kbd>F</kbd> <br> <kbd>F11</kbd> <br> <kbd>Alt + Return</kbd> <br> <kbd>Alt + Enter</kbd> | Fullscreen image viewer
<kbd>H</kbd> <br> <kbd>Left</kbd> <br> <kbd>Alt + H</kbd> <br> <kbd>Alt + Left</kbd> | Pan image left
<kbd>J</kbd> <br> <kbd>Down</kbd> <br> <kbd>Alt + J</kbd> <br> <kbd>Alt + Down</kbd> | Pan image down
<kbd>K</kbd> <br> <kbd>Up</kbd> <br> <kbd>Alt + K</kbd> <br> <kbd>Alt + Up</kbd> | Pan image up
<kbd>L</kbd> <br> <kbd>Right</kbd> <br> <kbd>Alt + L</kbd> <br> <kbd>Alt + Right</kbd> | Pan image right
<kbd>Z</kbd> <br> <kbd>+</kbd> <br> <kbd>Ctrl + +</kbd> | Zoom in
<kbd>Shift + Z</kbd> <br> <kbd>-</kbd> <br> <kbd>Ctrl + -</kbd> | Zoom out
<kbd>Alt + Z</kbd> <br> <kbd>=</kbd> <br> <kbd>Ctrl + =</kbd> | Reset zoom
<kbd>R</kbd> | Rotate image right
<kbd>Shift + R</kbd> | Rotate image left
<kbd>Alt + R</kbd> | Reset image rotation
<kbd>S</kbd> | Speed up gif
<kbd>Shift + S</kbd> | Slow down gif
<kbd>Alt + S</kbd> | Reset gif speed
<kbd>Space</kbd> | Pause gif

## Security tab bindings

Key | Function
------ | ------
<kbd>Tab</kbd> | Navigate next
<kbd>Shift + Tab</kbd> | Navigate previous
<kbd>Space</kbd> | Toggle check
<kbd>Menu</kbd> | Session context menu
<kbd>Alt + R</kbd> <br> <kbd>F5</kbd> | Refresh session list
<kbd>Alt + S</kbd> <br> <kbd>Delete</kbd> | Sign out session

*Binding different than in Mirage
